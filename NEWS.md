# mapfactory 0.0.0.9000

* Ajout des fonctions de création de carte : 

  - `creer_carte_epci()`
  - `creer_carte_epci_prop()`
  - `creer_carte_categorie_epci()`
  - `creer_carte_communes()`
  - `creer_carte_communes_prop()`
  - `creer_carte_categorie_communes()`

* Added a `NEWS.md` file to track changes to the package.
