test_that("format_fr_pct fonctionne", {
  testthat::expect_error(format_fr_pct("2"))
  testthat::expect_true(is.character(format_fr_pct(2)))
})


test_that("format_fr_nb fonctionne", {
  testthat::expect_error(format_fr_nb("2"))
  testthat::expect_true(is.character(format_fr_nb(2.9)))
})
