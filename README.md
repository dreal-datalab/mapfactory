
<!-- README.md is generated from README.Rmd. Please edit that file -->

# mapfactory <img src='man/figures/logo.png' align="right" height="139" />

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![pipeline
status](https://gitlab.com/dreal-datalab/mapfactory/badges/master/pipeline.svg)](https://gitlab.com/dreal-datalab/mapfactory/-/commits)
[![coverage
report](https://gitlab.com/dreal-datalab/mapfactory/badges/master/coverage.svg)](https://dreal-datalab.gitlab.io/mapfactory/coverage.html)

<!-- badges: end -->

Le but de {[`mapfactory`](https://dreal-datalab.gitlab.io/mapfactory/)}
est de faciliter la création de carte statistiques sur les territoires
administratifs français sur la base du package {`COGiter`}

## Installation

Vous pouvez installer {`mapfactory`} depuis gitlab.com:

``` r
remotes::install_gitlab("dreal-datalab/mapfactory", build_vignettes = TRUE)
```

## Exemple

Voilà comment créer une carte à l’EPCI sur la base de données communales
de 2015 à jour de la carte des territoires.

``` r
library(mapfactory)
library(COGiter)
pop2015_cog <- cogifier(pop2015)
fond_carto <- fond_carto(nom_reg = "Pays de la Loire")
creer_carte_communes(data = pop2015_cog,
                        code_region = "52",
                        carto = fond_carto,
                        indicateur = pop2015,
                        pourcent = FALSE,
                        decimales=0,
                        suffixe = "habitants",
                        interactive = FALSE,
                        method = "q6",
                        titre = "Population par communes",
                        sous_titre = "en 2015",
                        bas_de_page = "source : INSEE - RP"
)
```

<img src="man/figures/README-example-1.png" width="100%" />

## Documentation

Consulter [le site de documentation du
package](https://dreal-datalab.gitlab.io/mapfactory/).
